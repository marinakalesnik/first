package constants;

public class Constants {

    public static class Urls {
        public static final String ESHOP_LOGIN_URL = "http://open-eshop.stqa.ru/oc-panel/auth/login/";
    }

    public static class eShopTestData {

        /**
         * Constants for positive tests
         * */

        /**Data for Login*/
        public static final String ESHOP_LOGIN = "demo@open-eshop.com";
        public static final String ESHOP_PASSWORD = "demo";

        /**Data for creating a new coupon*/
        public static final String COUPON_NAME = "IRON MAN";
        public static final String DISCOUNT_AMOUNT = "10";
        public static final String VALID_DATE = "2022-01-28";
        public static final String NUMBER_OF_COUPONS = "1";

        /**Data to search for a new coupon*/
        public static final String SEARCH_COUPON = COUPON_NAME;
        public static final String COUPONS_COUNT_AFTER_SEARCH = "1";

        /**
         * Constants for negative tests
         * */

        /**Data for Login*/
        public static final String INCORRECT_ESHOP_LOGIN = "demo@open-eshop.com";
        public static final String INCORRECT_ESHOP_PASSWORD = "";

        /**Data for creating a new coupon*/
        public static final String INCORRECT_COUPON_NAME = "";
        public static final String INCORRECT_DISCOUNT_AMOUNT = "1000";
        public static final String INCORRECT_VALID_DATE = "28-01-202a";
        public static final String INCORRECT_NUMBER_OF_COUPONS = "0";

        /**Data to search for a new coupon*/
        public static final String SEARCH_INVALID_COUPON = INCORRECT_COUPON_NAME;

        /**
         *information messages
         */
        public static final String ERROR_LOGIN_Some_errors_in_the_form = "Some errors in the form";
        public static final String Message_SUCCSSES_Create_New_Coupon = "Success";

    }
}