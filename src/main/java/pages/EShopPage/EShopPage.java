package pages.EShopPage;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import constants.Constants;
import pages.Base.BasePage;

import static com.codeborne.selenide.Selenide.*;

public class EShopPage extends BasePage {

    /**Check is auth frame is visible*/
    public EShopPage isAuthWidgetPresent(){
        sleep(1000);
        authWidget.shouldBe(Condition.visible);
        return page(EShopPage.class);
    }

    /**Data for Login*/
    private final SelenideElement authWidget = $x("//div[@class='row']//child::form[@method='post']");
    private static final SelenideElement inputEmail = $x("//section[@id='page']//child::input[@name='email']");
    private final SelenideElement inputPassword = $x("//section[@id='page']//child::input[@name='password']");
    private final SelenideElement buttonLogin = $x("//div[@id='main']//child::form[@class='well form-horizontal auth']//child::button[@type='submit']"); /*$x("//section[@id='page']//child::button[@type='submit']");*/

    /**Data to go to coupons */
    private final SelenideElement eShop = $x("//span[@class='glyphicon glyphicon-th']//parent::a[@data-toggle='collapse']");
    private final SelenideElement coupons = $x("//a[@href='https://open-eshop.stqa.ru/oc-panel/coupon']");
    private final SelenideElement ButtonNewCoupon = $x("//a[@href='https://open-eshop.stqa.ru/oc-panel/Coupon/create']");

    /**Data for Logout*/
    private final SelenideElement DropDownAuthMenu = $x("//a[@class='btn dropdown-toggle btn-success navbar-btn']");
    private final SelenideElement buttonLogout = $x("//a[@href='https://open-eshop.stqa.ru/oc-panel/auth/logout']");

    /**Data for errors*/
    private final SelenideElement errorLoginWidget = $x("//section[@class='col-lg-9']//child::div[@class='alert alert-danger']");
    private final SelenideElement errorTextLoginWidget = $x("//section[@class='col-lg-9']//child::h4[@class='alert-heading']");


    /**Enter email in Login*/
    public EShopPage enterEmail(String email) {
        clearAndType(inputEmail, email);
        return this;
    }
    /**Enter password in Login*/
    public EShopPage enterPassword(String password) {
        clearAndType(inputPassword, password);
        return this;
    }
    /**Confirm Login*/
    public EShopPage submitLogin(){
        buttonLogin.shouldBe(Condition.visible).click();
        return this;
    }
    /**Click button eShop on PANEL*/
    public EShopPage clickButtonEShop(){
        eShop.shouldBe(Condition.visible).click();
        return this;
    }
    /**Click the Coupons button in the eShop section on the PANEL*/
    public EShopPage clickButtonCoupons(){
        coupons.shouldBe(Condition.visible).click();
        return this;
    }
    /**Click button NEW on the page Coupons*/
    public EShopPage clickButtonNewCoupon(){
        ButtonNewCoupon.shouldBe(Condition.visible).click();
        return this;
    }
    /**Click dropdown Auth menu*/
    public EShopPage clickDropDownAuthMenu(){
        DropDownAuthMenu.shouldBe(Condition.visible).click();
        return this;
    }
    /**Click Logout button*/
    public EShopPage ClickLogoutButton(){
        buttonLogout.shouldBe(Condition.visible).click();
        return this;
    }
    /**Check if error login widget present*/

    public EShopPage ErrorLoginWidgetPresent(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        errorLoginWidget.shouldBe(Condition.visible);
        return this;
    }
    /**Check error logins text*/
    public EShopPage checkErrorTextLoginWidget (String text){
        errorTextLoginWidget
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text(Constants.eShopTestData.ERROR_LOGIN_Some_errors_in_the_form));
        return this;
    }
}