package pages.EShopPage;

import com.codeborne.selenide.*;
import constants.Constants;
import pages.Base.BasePage;

import static com.codeborne.selenide.Selenide.*;
import static constants.Constants.eShopTestData.COUPONS_COUNT_AFTER_SEARCH;
import static constants.Constants.eShopTestData.SEARCH_COUPON;

public class CouponsPage extends BasePage {


    /**Check is create coupon frame is visible*/
    public CouponsPage IsCreateCouponWidgetPresent(){
        sleep(1000);
        CreateCouponWidget.shouldBe(Condition.visible);
        return page(CouponsPage.class);
    }
    /**Check is alert delete coupon frame is visible*/
    public CouponsPage IsAlertDeleteCouponPresent(){
        sleep(1000);
        AlertDeleteCoupon.shouldBe(Condition.visible);
        return page(CouponsPage.class);
    }

    /**Data for creating a new coupon*/
    private final SelenideElement CreateCouponWidget = $x("//div[@class = 'col-md-6']");
    private final SelenideElement inputCouponName = $x("//input[@id='name']");
    private final SelenideElement inputDiscountAmount = $x("//input[@id='discount_amount']");
    private final SelenideElement inputValidDate = $x("//input[@name='valid_date']");
    private final SelenideElement inputNumberOfCoupons = $x("//input[@id='number_coupons']");
    private final SelenideElement buttonSUBMIT = $x("//button[@type='submit']");
    private final SelenideElement messageSuccessCreateNewCoupon = $x("//h4[@class='alert-heading']");

    /**Data to search for a new coupon*/
    private final SelenideElement inputSearch = $x("//input[@type='text']");
    private final SelenideElement buttonSearch = $x("//form[@class='form-inline pull-right']//child::button[@type='submit']");
    private final SelenideElement nameOfFoundCoupon = $x("//div[@class='table-responsive']/child::table[@class='table table-striped table-bordered']/tbody/tr/td");
    private final ElementsCollection CouponsTable = $$x("//div[@class='table-responsive']/child::table[@class='table table-striped table-bordered']/tbody/tr");

    /**Data to delete for a new coupon*/
    private final SelenideElement DeleteCouponButton = $x("//a[@class='btn btn-danger index-delete']");
    private final SelenideElement AlertDeleteCoupon = $x("//div[@class = 'sweet-alert showSweetAlert visible']");
    private final SelenideElement ConfirmDeleteCouponButton = $x("//button[@class = 'confirm']");


    /**Enter coupon name*/
    public CouponsPage enterCouponName(String CouponName){
        clearAndType(inputCouponName, CouponName);
        return this;
    }
    /**Enter Discount Amount*/
    public CouponsPage enterDiscountAmount(String Amount){
        clearAndType(inputDiscountAmount, Amount);
        return this;
    }
    /**Enter valid date*/
    public CouponsPage enterValidDate(String ValidDate){
        clearAndType(inputValidDate, ValidDate);
        return this;
    }
    /**Enter number of coupons*/
    public CouponsPage enterNumberOfCoupons(String NumberOfCoupons){
        clearAndType(inputNumberOfCoupons, NumberOfCoupons);
        return this;
    }
    /**Click submit button to create a new coupon*/
    public CouponsPage clickButtonSUBMIT(){
        buttonSUBMIT.shouldBe(Condition.visible).click();
        return this;
    }
    /**Enter the name of the coupon in the search input*/
    public CouponsPage enterSearch(String DataSearch){
        clearAndType(inputSearch, DataSearch);
        return this;
    }
    /**Click search button to find a new coupon*/
    public CouponsPage clickButtonSearch(){
        buttonSearch.shouldBe(Condition.visible).click();
        return this;
    }
    /**Click delete coupon button */
    public CouponsPage clickButtonDelete(){
        DeleteCouponButton.shouldBe(Condition.visible).click();
        return this;
    }
    /**Click confirm delete coupon button*/
    public CouponsPage confirmDeleteCoupon(){
        ConfirmDeleteCouponButton.shouldBe(Condition.visible).click();
        return this;
    }
    /**Message Success of create new coupon is visible*/
    public CouponsPage messageSuccessIsVisible(){
        messageSuccessCreateNewCoupon
                .shouldBe(Condition.visible)
                .shouldHave(Condition.text(Constants.eShopTestData.Message_SUCCSSES_Create_New_Coupon));
        return this;
    }

    /**Search coupon is equal to found coupon*/
    public CouponsPage SearchCouponIsEqualToFoundCoupon(){
        nameOfFoundCoupon.shouldBe(Condition.visible);
        equals(SEARCH_COUPON);
        return this;
    }

    /**Check is after searching available one coupon*/
    public CouponsPage CheckAfterSearchingAvailableOneCoupon(){
        CouponsTable.shouldHave(CollectionCondition.size(Integer.parseInt(COUPONS_COUNT_AFTER_SEARCH)));
        return this;
    }

    /**Check if table is hidden after removing coupon*/
    public CouponsPage checkIsTableHiddenAfterCouponsDelete(){
        nameOfFoundCoupon.shouldBe(Condition.hidden);
        return this;
    }

}
