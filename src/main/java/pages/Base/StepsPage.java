package pages.Base;

import pages.EShopPage.CouponsPage;
import pages.EShopPage.EShopPage;
import static com.codeborne.selenide.Selenide.sleep;
import static constants.Constants.Urls.ESHOP_LOGIN_URL;
import static constants.Constants.eShopTestData.*;
import static constants.Constants.eShopTestData.NUMBER_OF_COUPONS;

public class StepsPage extends EShopPage {

    protected EShopPage eShopPage = new EShopPage();
    protected CouponsPage couponsPage = new CouponsPage();
    protected BasePage basePage = new BasePage();

    /**Step for navigating to a specific URL*/
    public void goToURLStep() {
        basePage.GoToUrl(ESHOP_LOGIN_URL);
    }

    /**Step for login*/
    public void loginStep() {
        eShopPage
                .isAuthWidgetPresent()
                .enterEmail(ESHOP_LOGIN)
                .enterPassword(ESHOP_PASSWORD)
                .submitLogin();
    }

    /**Step for check incorrect login*/
    public void incorrectLoginStep() {
        eShopPage
                .enterEmail(ESHOP_LOGIN)
                .enterPassword(ESHOP_PASSWORD)
                .submitLogin();
    }

    /**Step for check message error login*/
    public void checkErrorLoginStep() {
        eShopPage
                .isAuthWidgetPresent()
                .enterEmail(INCORRECT_ESHOP_LOGIN)
                .enterPassword(INCORRECT_ESHOP_PASSWORD)
                .submitLogin()
                .ErrorLoginWidgetPresent()
                .checkErrorTextLoginWidget(ERROR_LOGIN_Some_errors_in_the_form);
    }

    /**Step for check enter incorrect coupon valid data*/
    public void checkIncorrectCouponValidDataStep(){
        eShopPage.clickButtonNewCoupon();
        couponsPage
                .IsCreateCouponWidgetPresent()
                .enterCouponName(COUPON_NAME)
                .enterDiscountAmount(DISCOUNT_AMOUNT)
                .enterValidDate(INCORRECT_VALID_DATE)
                .enterNumberOfCoupons(NUMBER_OF_COUPONS)
                .clickButtonSUBMIT();
    }

    /**Step to go to coupons page*/
    public void goToPageCouponsStep() {
        eShopPage
                .clickButtonEShop()
                .clickButtonCoupons();
    }

    /**Step to create a new coupon*/
    public void createNewCouponStep() {
        eShopPage.clickButtonNewCoupon();
        couponsPage
                .IsCreateCouponWidgetPresent()
                .enterCouponName(COUPON_NAME)
                .enterDiscountAmount(DISCOUNT_AMOUNT)
                .enterValidDate(VALID_DATE)
                .enterNumberOfCoupons(NUMBER_OF_COUPONS)
                .clickButtonSUBMIT();
    }

    /**Step to search a new coupon*/
    public void searchNewCouponStep() {
        couponsPage
                .enterSearch(SEARCH_COUPON)
                .clickButtonSearch();
    }

    /**Step to delete a new coupon*/
    public void deleteNewCouponStep() {
        sleep(1000);
        couponsPage
                .clickButtonDelete()
                .IsAlertDeleteCouponPresent()
                .confirmDeleteCoupon();
    }

    /**Step to check message  for successfully added coupon*/
    public void checkMessageSuccessCreateNewCouponStep() {
        couponsPage.messageSuccessIsVisible();
    }

    /**Step for logout*/
    public void logOutStep() {
        eShopPage
                .clickDropDownAuthMenu()
                .ClickLogoutButton();
    }

    /**Step for check: is search coupon equal to found coupon*/
    public void searchCouponIsEqualToFoundCouponStep(){
        couponsPage.SearchCouponIsEqualToFoundCoupon();
    }
}
