package Common;
import com.codeborne.selenide.Configuration;

public class Config {

    /**Specify the browser and platform for test:*/
    public static final String BROWSER_NAME = "chrome";

    /**Clean browser cookies after each iteration*/
    public static final Boolean CLEAR_COOKIES = false;

    /** To keep the browser open after all scenario/tests*/
    public static final Boolean HOLD_BROWSER_OPEN = true;

    /** Clear the directory with the screen before starting the build*/
    public static final Boolean CLEAR_REPORTS_DIR = true;


    static {
        Configuration.browser = BROWSER_NAME;
        Configuration.holdBrowserOpen = HOLD_BROWSER_OPEN;

    }
}
