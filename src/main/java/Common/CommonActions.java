package Common;

import com.codeborne.selenide.Selenide;

import static Common.Config.CLEAR_COOKIES;

public class CommonActions {

    private static void clearBrowserCookieAndStorage(){
        if (CLEAR_COOKIES) {
            try {
                Selenide.clearBrowserCookies();
                Selenide.clearBrowserLocalStorage();
                Selenide.executeJavaScript("window.sessionStorage.clear()");
            } catch (Exception e) {
            }
        }
    }
}