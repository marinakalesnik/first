package tests.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.Base.BasePage;
import pages.EShopPage.CouponsPage;
import pages.EShopPage.EShopPage;
import java.io.File;
import java.time.LocalTime;
import java.util.Objects;

import static Common.Config.CLEAR_REPORTS_DIR;

public class BaseTest {

    protected BasePage basePage = new BasePage();
    protected EShopPage eShopPage = new EShopPage();
    protected CouponsPage couponsPage = new CouponsPage();
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseTest.class);

    /**
     * A static initialization block in order to clean the folders with reports and screenshots before starting the build
     */

    static {
        LOGGER.info("START TIME!" + LocalTime.now());
        LOGGER.info("Start clear reports dir: build/reports...");
        File allureResults = new File("allure-results");
        if (allureResults.isDirectory()) {
            for (File item : Objects.requireNonNull(allureResults.listFiles())) {
                item.delete();
            }
        }
        if (CLEAR_REPORTS_DIR) {
            File allureScreenShots = new File("build/reports/tests");
            if (allureResults.isDirectory()) {
                for (File item : Objects.requireNonNull(allureScreenShots.listFiles())) {
                    item.delete();
                }
            }
            File downloads = new File("build/downloads");
            if (downloads.isDirectory()) {
                for (File item : Objects.requireNonNull(allureScreenShots.listFiles())) {
                    item.delete();
                }
            }
        }
    }
}
