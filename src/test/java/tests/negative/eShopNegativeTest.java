package tests.negative;

import org.junit.jupiter.api.Test;
import pages.Base.StepsPage;
import tests.base.BaseTest;

public class eShopNegativeTest extends BaseTest {

    protected StepsPage stepsPage = new StepsPage();

    @Test
    public void incorrectLogin(){
        stepsPage.goToURLStep();
        stepsPage.incorrectLoginStep();
    }

    @Test
    public void checkErrorLogin(){
        stepsPage.goToURLStep();
        stepsPage.checkErrorLoginStep();
    }

    @Test
    public void checkIncorrectCouponValidData(){
        stepsPage.goToURLStep();
        stepsPage.loginStep();
        stepsPage.goToPageCouponsStep();
        stepsPage.checkIncorrectCouponValidDataStep();
    }















     /* @Test
    public void GoToURL_Login() {
        basePage.GoToUrl(ESHOP_LOGIN_URL);
        eShopPage
                .isAuthWidgetPresent()
                .enterEmail(ESHOP_LOGIN)
                .enterPassword(ESHOP_PASSWORD)
                .submitLogin();
    }*/

    /*@Test
    public void checkErrorLogin() {
        stepsPage.goToURLStep();
        stepsPage.incorrectLoginStep();

        eShopPage
                .isAuthWidgetPresent()
                .enterEmail(INCORRECT_ESHOP_LOGIN)
                .enterPassword(INCORRECT_ESHOP_PASSWORD)
                .submitLogin()
                .ErrorLoginWidgetPresent()
                .checkErrorTextLoginWidget(ERROR_LOGIN_Some_errors_in_the_form);
    }*/

    /*@Test
    public void checkIncorrectValidData(){
        eShopPage
                .clickButtonEShop()
                .clickButtonCoupons()
                .clickButtonNewCoupon();
        couponsPage
                .IsCreateCouponWidgetPresent()
                .enterCouponName(COUPON_NAME)
                .enterDiscountAmount(DISCOUNT_AMOUNT)
                .enterValidDate(INCORRECT_VALID_DATE)
                .enterNumberOfCoupons(NUMBER_OF_COUPONS)
                .clickButtonSUBMIT();
    }*/
}
