package tests.positive;

import pages.Base.StepsPage;
import tests.base.BaseTest;
import org.junit.jupiter.api.Test;

public class eShopPositiveTest extends BaseTest {

    protected StepsPage stepsPage = new StepsPage();

    @Test
    public void goToURL(){
        stepsPage.goToURLStep();
    }

    @Test
    public void checkLogin(){
        stepsPage.goToURLStep();
        stepsPage.loginStep();
    }

    @Test
    public void createNewCoupon(){
        stepsPage.goToURLStep();
        stepsPage.loginStep();
        stepsPage.goToPageCouponsStep();
        stepsPage.createNewCouponStep();
    }

    @Test
    public void searchNewCoupon(){
        stepsPage.goToURLStep();
        stepsPage.loginStep();
        stepsPage.goToPageCouponsStep();
        stepsPage.createNewCouponStep();
        stepsPage.searchNewCouponStep();
    }

    @Test
    public void deleteNewCoupon(){
        stepsPage.goToURLStep();
        stepsPage.loginStep();
        stepsPage.goToPageCouponsStep();
        stepsPage.searchNewCouponStep();
        stepsPage.deleteNewCouponStep();
    }

    @Test
    public void checkMessageSuccessCreateNewCoupon(){
        stepsPage.goToURLStep();
        stepsPage.loginStep();
        stepsPage.goToPageCouponsStep();
        stepsPage.createNewCouponStep();
        stepsPage.checkMessageSuccessCreateNewCouponStep();
    }

    @Test
    public void logOut(){
        stepsPage.goToURLStep();
        stepsPage.loginStep();
        stepsPage.logOutStep();
    }

    @Test
    public void searchCouponIsEqualToFoundCoupon(){
        stepsPage.goToURLStep();
        stepsPage.loginStep();
        stepsPage.goToPageCouponsStep();
        stepsPage.createNewCouponStep();
        stepsPage.searchNewCouponStep();
        stepsPage.searchCouponIsEqualToFoundCouponStep();
    }

    @Test
    public void checkAfterSearchingAvailableOneCoupon(){
        stepsPage.goToURLStep();
        stepsPage.loginStep();
        stepsPage.goToPageCouponsStep();
        stepsPage.createNewCouponStep();
        stepsPage.searchNewCouponStep();
        stepsPage.searchCouponIsEqualToFoundCouponStep();
        couponsPage.CheckAfterSearchingAvailableOneCoupon();
    }

    @Test
    public void checkIsTableHiddenAfterCouponsDelete(){
        stepsPage.goToURLStep();
        stepsPage.loginStep();
        stepsPage.goToPageCouponsStep();
        stepsPage.createNewCouponStep();
        stepsPage.searchNewCouponStep();
        stepsPage.deleteNewCouponStep();
        couponsPage.checkIsTableHiddenAfterCouponsDelete();
    }

    /**Main test*/
    @Test
    public void mainTest(){
        stepsPage.goToURLStep();
        stepsPage.loginStep();
        stepsPage.goToPageCouponsStep();
        stepsPage.createNewCouponStep();
        stepsPage.searchNewCouponStep();
        stepsPage.deleteNewCouponStep();
        stepsPage.logOutStep();
    }












       /* @Test
    public void GoToURL(){
        basePage.GoToUrl(ESHOP_LOGIN_URL);
    }
*/

      /* @Test
    public void checkLogin(){
        GoToURL();
        eShopPage
                .isAuthWidgetPresent()
                .enterEmail(ESHOP_LOGIN)
                .enterPassword(ESHOP_PASSWORD)
                .submitLogin();
    }*/

    /*    @Test
    public void createNewCoupon(){
        checkLogin();
        eShopPage
                .clickButtonEShop()
                .clickButtonCoupons()
                .clickButtonNewCoupon();

        couponsPage
                .IsCreateCouponWidgetPresent()
                .enterCouponName(COUPON_NAME)
                .enterDiscountAmount(DISCOUNT_AMOUNT)
                .enterValidDate(VALID_DATE)
                .enterNumberOfCoupons(NUMBER_OF_COUPONS)
                .clickButtonSUBMIT();
    }*/

/*    @Test
    public void SearchNewCoupon(){
        createNewCoupon();
        couponsPage
                .enterSearch(SEARCH_COUPON)
                .clickButtonSearch();
    }*/

    /*    @Test
    public void DeleteNewCoupon() {
        SearchNewCoupon();
        sleep(1000);

        couponsPage
                .clickButtonDelete()
                .IsAlertDeleteCouponPresent()
                .confirmDeleteCoupon();
    }*/

        /*@Test
    public void CheckMessageSuccessCreateNewCoupon(){
        createNewCoupon();
        couponsPage.messageSuccessIsVisible();
    }*/

    /* @Test
    public void LogOut(){
        DeleteNewCoupon();
        eShopPage
                .clickDropDownAuthMenu()
                .ClickLogoutButton();
    }*/

     /*@Test
    public void SearchCouponIsEqualToFoundCoupon(){
        SearchNewCoupon();
        couponsPage.SearchCouponIsEqualToFoundCoupon();

    }*/

     /*@Test
    public void CheckAfterSearchingAvailableOneCoupon(){
        SearchCouponIsEqualToFoundCoupon();
        couponsPage.CheckAfterSearchingAvailableOneCoupon();
    }*/

    /*@Test
    public void CheckIsTableHiddenAfterCouponsDelete(){
        DeleteNewCoupon();
        couponsPage.checkIsTableHiddenAfterCouponsDelete();
    }*/
}
